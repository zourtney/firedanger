﻿using Microsoft.Phone.Globalization;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace FireDanger.Helpers
{
    public class UniqueKeyGroup<T> : List<T>
    {
        const string GlobeGroupKey = "\uD83C\uDF10";

        /// <summary>
        /// The Key of this group.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Public ctor.
        /// </summary>
        /// <param name="key">The key for this group.</param>
        public UniqueKeyGroup(string key)
        {
            Key = key;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping 
        /// using the current threads culture to determine which alpha keys to
        /// include.
        /// </summary>
        /// <param name="items">The items to place in the groups.</param>
        /// <param name="getKey">A delegate to get the key from an item.</param>
        /// <param name="sort">Will sort the data if true.</param>
        /// <returns>An items source for a LongListSelector</returns>
        public static List<UniqueKeyGroup<T>> CreateGroups(IEnumerable<T> items, Func<T, string> keySelector, bool sort)
        {
            List<UniqueKeyGroup<T>> list = new List<UniqueKeyGroup<T>> ();

            foreach (T item in items)
            {
                string key = keySelector(item);
                int index = list.FindIndex((UniqueKeyGroup<T> k) => k.Key == key);

                if (index >= 0 && index < list.Count)
                {
                    list[index].Add(item);
                }
                else
                {
                    list.Add(new UniqueKeyGroup<T>(key));
                }
            }

            //TODO: fix this. The sorting doesnt work at all.
            if (sort)
            {
                foreach (UniqueKeyGroup<T> group in list)
                {
                    group.Sort((c0, c1) => { return keySelector(c0).CompareTo(keySelector(c1)); });
                }
            }

            return list;
        }
    }
}
