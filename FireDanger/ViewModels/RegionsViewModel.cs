﻿using FireDanger.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WildlandFire;

namespace FireDanger.ViewModels
{
    public class RegionsViewModel : INotifyPropertyChanged // : ObservableCollection <Models.Region>
    {
        public RegionsViewModel() { }

        public bool IsDataLoaded { get; private set; }
        protected WildlandFire.WildlandFire Service { get; set; }

        /// <summary>
        /// Creates and adds a few Person objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            this.Service = new WildlandFire.WildlandFire();
            
            // Wire up event listeners
            this.Service.RequestBegin += service_RequestBegin;
            this.Service.RequestComplete += service_RequestComplete;
            this.Service.ParseComplete += service_ParseComplete;

            // Get and parse data from external WSAF source
            this.Service.Fetch();
            //service_ParseComplete(null, EventArgs.Empty);
        }

        void service_RequestBegin(object obj, EventArgs e)
        {
            //TODO: show loader animation
        }

        void service_RequestComplete(object obj, EventArgs e)
        {
            //TODO: hide loader animation. Show "parsing" indicator.
        }

        void service_ParseComplete(object obj, EventArgs e)
        {
            //TODO: not this. Use property change events or don't. Stop misusing them.
            NotifyPropertyChanged("Service");
            NotifyPropertyChanged("GeneratedDateTime");
            NotifyPropertyChanged("UpdatedDateTime");
        }

        internal List<UniqueKeyGroup<Models.Region>> GetByState()
        {
            var keys = (
                        from st in this.Service.States
                        orderby st.Key
                        select new UniqueKeyGroup<Models.Region>(st.Key)
                       );

            var list = keys.ToList();
            foreach (UniqueKeyGroup<Models.Region> state in list)
            {
                state.AddRange(
                               from r in this.Service.States[state.Key]
                               orderby r.Key
                               select new Models.Region(r.Key, state.Key, r.Value)
                              );
            }

            return list;
        }

        public string GeneratedDateTime
        {
            get
            {
                var d = this.Service.GeneratedDateTime;
                var timeZone = (this.Service.GeneratedTimeZone == "Mountain Time" ? "MST" : "");
                return d.ToShortDateString() + " at " + d.ToShortTimeString() + " " + timeZone;
            }
        }

        public string UpdatedDateTime
        {
            get
            {
                var d = this.Service.UpdatedDateTime;
                return d.ToShortDateString() + " at " + d.ToShortTimeString();
            }
        }

        internal Models.Region GetByName(string regionName, string stateName)
        {
            return new Models.Region(regionName, stateName, this.Service.States[stateName][regionName]);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            // Avoid `UnauthorizedAccessException` exceptions
            // http://info.titodotnet.com/2012/02/unauthorized-access-exception-when.html
            if (PropertyChanged != null)
            {
                if (Deployment.Current.Dispatcher.CheckAccess())
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    });
                }
            }
        }
    }
}
