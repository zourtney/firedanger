﻿using FireDanger.Helpers;
using FireDanger.Models;
using WildlandFire;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace FireDanger.ViewModels
{
    public class LocationsViewModel : INotifyPropertyChanged
    {
        private List<Location> _locations;

        public LocationsViewModel()
        {
            this.Locations = new List<Location>();
            this.LoadData();
        }


        /// <summary>
        /// A collection for Location objects.
        /// </summary>
        public List<Location> Locations
        {
            get
            {
                return _locations;
            }
            private set
            {
                _locations = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// A collection for Locations objects grouped by their state.
        /// </summary>
        public List<UniqueKeyGroup<FireDanger.Models.State>> GroupedLocations
        {
            get
            {
                return null;
                //return UniqueKeyGroup<Location>.CreateGroups(
                //    Locations,
                //    (Location s) => { return s.State; },
                //    true);
                
            }
        }

        public bool IsDataLoaded { get; private set; }
        protected WildlandFire.WildlandFire Service { get; set; }

        /// <summary>
        /// Creates and adds a few Person objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            this.Service = new WildlandFire.WildlandFire();
            
            // Wire up event listeners
            this.Service.RequestBegin += service_RequestBegin;
            this.Service.RequestComplete += service_RequestComplete;
            this.Service.ParseComplete += service_ParseComplete;

            // Get and parse data from external WSAF source
            this.Service.Fetch();
            //service_ParseComplete(null, EventArgs.Empty);
        }

        void service_RequestBegin(object obj, EventArgs e)
        {
            //TODO: show loader animation
        }

        void service_RequestComplete(object obj, EventArgs e)
        {
            //TODO: hide loader animation. Show "parsing" indicator.
        }

        void service_ParseComplete(object obj, EventArgs e)
        {

            this.Locations.Add(new Location() { State = "California", Region = "BLUE RIDGE" });
            this.Locations.Add(new Location() { State = "California", Region = "MT SHASTA" });
            this.Locations.Add(new Location() { State = "California", Region = "OAK KNOLL" });
            this.Locations.Add(new Location() { State = "California", Region = "SAWYERS BAR" });
            this.Locations.Add(new Location() { State = "California", Region = "SLATER BUTTE" });
            this.Locations.Add(new Location() { State = "California", Region = "WEED AIRPORT" });

            this.Locations.Add(new Location() { State = "Colorado", Region = "LADORE" });
            this.Locations.Add(new Location() { State = "Colorado", Region = "DINOSAUR NM SUCCES" });
            this.Locations.Add(new Location() { State = "Colorado", Region = "GREAT DIVIDE" });

            this.Locations.Add(new Location() { State = "Oregon", Region = "NORTH POLE" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "PATJENS" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "HAYSTACK" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "BOARD HOLLOW" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "SLIDE" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "BRER RABBIT" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "MORGAN MT" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "ROUND MOUNTAIN" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "LAVA BUTTE" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "COLGATE" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "TEPEE DRAW" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "COLD SPRINGS" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "BADGER" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "SALT CREEK" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "BLACK ROCK" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "CABIN LAKE" });
            this.Locations.Add(new Location() { State = "Oregon", Region = "BROWNS WELL" });

            this.Locations.Add(new Location() { State = "Arizona", Region = "TUSAYAN" });
            this.Locations.Add(new Location() { State = "Arizona", Region = "BRIGHT ANGEL" });
            this.Locations.Add(new Location() { State = "Arizona", Region = "DRYPARK" });
            this.Locations.Add(new Location() { State = "Arizona", Region = "WARM SPRINGS" });

            this.Locations.Add(new Location() { State = "Arkansas", Region = "POINSETT" });
            this.Locations.Add(new Location() { State = "Arkansas", Region = "GUY" });
            this.Locations.Add(new Location() { State = "Arkansas", Region = "SHERIDAN" });
            this.Locations.Add(new Location() { State = "Arkansas", Region = "BLUFF CITY" });
            this.Locations.Add(new Location() { State = "Arkansas", Region = "UAM" });

            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}