﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using FireDanger.Resources;
using FireDanger.Models;
using FireDanger.Helpers;
using System.Collections.Generic;
using WildlandFire;
using System.Windows;
using System.IO.IsolatedStorage;

namespace FireDanger.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private Models.Region _currentRegion;
        private IsolatedStorageSettings _userSettings = IsolatedStorageSettings.ApplicationSettings;
        
        public MainViewModel()
        {
            //this.Items = new ObservableCollection<ItemViewModel>();
            this.RegionsByState = new List<UniqueKeyGroup<Models.Region>>();
            
            this.Regions = new RegionsViewModel();
            this.Regions.PropertyChanged += _regions_PropertyChanged;
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        //public ObservableCollection<ItemViewModel> Items { get; private set; }

        public List<UniqueKeyGroup<Models.Region>> RegionsByState { get; private set; }
        public RegionsViewModel Regions { get; private set; }
        public FireDanger.Models.Region CurrentRegion
        {
            get
            {
                return _currentRegion;
            }
            set
            {
                _currentRegion = value;

                // Save to application settings
                _userSettings["region"] = value.Name;
                _userSettings["state"] = value.State;
                
                NotifyPropertyChanged("CurrentRegion");
            }
        }

        public string LastUpdated { get; private set; }
        public bool IsDataLoaded { get; private set; }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            // Sample data; replace with real data
            //this.Items.Add(new ItemViewModel() { LineOne = "WFAS", LineTwo = "Wildland Fire Assessment System", LineThree = "Facilisi faucibus habitant inceptos interdum lobortis nascetur pharetra placerat pulvinar sagittis senectus sociosqu" });
            this.Regions.LoadData();

            this.IsDataLoaded = true;
        }

        void _regions_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RegionsViewModel data = (RegionsViewModel)sender;

            this.RegionsByState = data.GetByState();
            NotifyPropertyChanged("RegionsByState");

            try
            {
                string stateName = (string)_userSettings["state"];
                string regionName = (string)_userSettings["region"];

                this.CurrentRegion = data.GetByName(regionName, stateName);
            }
            catch (KeyNotFoundException)
            {
                this.CurrentRegion = this.RegionsByState[0][0];
            }
              
            //NotifyPropertyChanged("CurrentRegion");

            //this.LastUpdated = this.Regions.LastUpdated;
            //NotifyPropertyChanged("LastUpdated");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            // Avoid `UnauthorizedAccessException` exceptions
            // http://info.titodotnet.com/2012/02/unauthorized-access-exception-when.html
            if (PropertyChanged != null)
            {
                if (Deployment.Current.Dispatcher.CheckAccess())
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    });
                }
            }
        }
    }
}