﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FireDanger.Models
{
    public class Region
    {
        protected WildlandFire.Region _region;
        protected static class Fields
        {
            public const string AdjustedDangerLevel = "ADJ";
            public const string EngeryReleaseComponent = "ERC";
            public const string Temperature = "Tmp";
            public const string RelativeHumidity = "RH";
            public const string Latitude = "Lat";
            public const string Longitude = "Long";
            public const string Wind = "Wind";
            public const string Precipitation = "PPT";
            public const string BurningIndex = "BI";
            public const string SpreadComponent = "SC";
            public const string DroughtIndex = "KBDI";
            public const string Elevation = "Elev";
        }

        public Region(string r, string s, WildlandFire.Region region)
        {
            this.Name = r.Trim();
            this.State = s.Trim();
            _region = region;
        }

        /// <summary>
        /// Return the raw name (will be all caps)
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Return the region's name, formatted with first-letter caps
        /// </summary>
        public string PrettyName
        {
            get
            {
                var parts = Regex.Split(this.Name, "\\s+");
                var ret = new StringBuilder("");
                string tmp;

                foreach (string part in parts)
                {
                    tmp = part.ToLowerInvariant();

                    ret.Append(tmp.Substring(0, 1).ToUpperInvariant())
                       .Append(tmp.Substring(1))
                       .Append(" ");
                }

                return ret.ToString().Trim();
            }
        }

        /// <summary>
        /// Return the state name
        /// </summary>
        public string State { get; private set; }

        /// <summary>
        /// Return the danger level as a friendly string
        /// </summary>
        public string DangerLevel
        {
            get
            {
                string val = _region[Fields.AdjustedDangerLevel];
                switch (val.Trim())
                {
                    case "L": return "Low";
                    case "M": return "Moderate";
                    case "H": return "High";
                    case "V": return "Extreme";
                }
                return "Unknown";
            }
        }

        /// <summary>
        /// Return the Engery Release Component index value as an integer
        /// </summary>
        public int EngeryReleaseComponent
        {
            get
            {
                return int.Parse(_region[Fields.EngeryReleaseComponent].Trim());
            }
        }

        public int Elevation
        {
            get
            {
                return int.Parse(_region[Fields.Elevation].Trim());
            }
        }

        /// <summary>
        /// Return the temperature
        /// </summary>
        public int Temperature
        {
            get
            {
                return int.Parse(_region[Fields.Temperature].Trim());
            }
        }

        /// <summary>
        /// Return the relative humidity (RH) 
        /// </summary>
        public int RelativeHumidity
        {
            get
            {
                return int.Parse(_region[Fields.RelativeHumidity].Trim());
            }
        }

        /// <summary>
        /// Return the latitude of the region
        /// </summary>
        public string Latitude
        {
            get
            {
                return _region[Fields.Latitude].Trim();
            }
        }
        
        /// <summary>
        /// Return the longitude of the region
        /// </summary>
        public string Longitude
        {
            get
            {
                return _region[Fields.Longitude].Trim();
            }
        }

        /// <summary>
        /// Return the wind speed
        /// </summary>
        public int Wind
        {
            get
            {
                return int.Parse(_region[Fields.Wind].Trim());
            }
        }

        /// <summary>
        /// Return the chance of precipitation
        /// </summary>
        public int Preciptiation
        {
            get
            {
                return (int)(double.Parse(_region[Fields.Precipitation].Trim()) * 100);
            }
        }

        /// <summary>
        /// Return the burning index value
        /// </summary>
        public int BurningIndex
        {
            get
            {
                return int.Parse(_region[Fields.BurningIndex].Trim());
            }
        }

        /// <summary>
        /// Return the spread component value
        /// </summary>
        public int SpreadComponent
        {
            get
            {
                return int.Parse(_region[Fields.SpreadComponent].Trim());
            }
        }

            /// <summary>
        /// Return the spread component value
        /// </summary>
        public int DroughtIndex
        {
            get
            {
                return int.Parse(_region[Fields.DroughtIndex].Trim());
            }
        }
    }
}
