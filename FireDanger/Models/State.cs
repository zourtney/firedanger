﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireDanger.Models
{
    public class State
    {
        public State(string n)
        {
            this.Name = n;
            this.Regions = new List<Region>();
        }

        public string Name { get; set; }
        public List<Region> Regions { get; private set; }
    }
}
