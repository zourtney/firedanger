﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FireDanger.Resources;
using FireDanger.Helpers;
using FireDanger.ViewModels;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;

namespace FireDanger
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }

            // Always show the main panel
            MainPivot.SelectedIndex = 0;

            // If we have "region" and "state" data in the query string, use
            // it for the app's initial state.
            var q = NavigationContext.QueryString;
            if (q.ContainsKey("region") && q.ContainsKey("state"))
            {
                IsolatedStorageSettings userSettings = IsolatedStorageSettings.ApplicationSettings;
                userSettings["region"] = Uri.UnescapeDataString(q["region"]);
                userSettings["state"] = Uri.UnescapeDataString(q["state"]);
            }
        }

        private void linkSettings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/RegionSelectorPage.xaml", UriKind.Relative));
        }

        private void linkAbout_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        private void linkPin_Click(object sender, EventArgs e)
        {
            try
            {
                Models.Region region = App.ViewModel.CurrentRegion;
                string queryStr = "/MainPage.xaml?region=" + Uri.EscapeDataString(region.Name) + "&state=" + Uri.EscapeDataString(region.State);
                var existingTile = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains(queryStr));

                if (existingTile == null)
                {
                    var navigationUri = new Uri(queryStr, UriKind.Relative);
                    var backImage = new Uri("/Assets/" + region.DangerLevel.ToLower() + ".png", UriKind.Relative);
                    
                    StandardTileData data = new StandardTileData
                    {
                        Title = region.PrettyName + ", " + region.State,
                        BackTitle = region.DangerLevel,
                        BackBackgroundImage = backImage
                    };

                    ShellTile.Create(navigationUri, data);
                }
                else
                {
                    // Tile already exists. Do something nice (like crossed out pin icon) later.
                    MessageBox.Show("Live tile already added");
                }
            }
            catch
            {
                MessageBox.Show("Unable to add live tile.");
            }
            
        }

        private void linkRefresh_Click(object sender, EventArgs e)
        {
            App.ViewModel.LoadData();
        }

        private void linkTest_Click(object sender, EventArgs e)
        {
            ScheduledActionService.LaunchForTest("MyTask", TimeSpan.FromMilliseconds(1500));
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}