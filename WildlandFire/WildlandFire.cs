using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace WildlandFire
{
    public class Region : Dictionary <string, string> {}
    public class State : Dictionary <string, Region> {}
    public class StateDictionary : Dictionary <string, State> {}

    public class WildlandFire
    {
        public event EventHandler RequestBegin;
        public event EventHandler RequestComplete;
        public event EventHandler ParseComplete;

        protected class ColumnHeader
        {
            public ColumnHeader(string n, int s, int e)
            {
                this.Name = n;
                this.StartIndex = s;
                this.EndIndex = e;
            }

            public string Name { get; set; }
            public int StartIndex { get; set; }
            public int EndIndex { get; set; }
        }

        protected class ColumnHeaderList : List<ColumnHeader> { };

        protected const string DATA_URL = "http://www.wfas.net/images/firedanger/fdr_obs.txt";
        //public const string DATA_URL = "http://localhost/wfas/Data.txt";
        //public const string DATA_URL = "http://localhost/wfas/fdr_obs.txt";
        protected const string GENERATED_DATETIME_PATTERN = "^([0-9]{2}-[A-Z]{3}-[0-9]{2}).*@\\s([0-9]{2})([0-9]{2})\\s(.*)$";
        protected const string STATE_PATTERN = "^\\*\\*\\*\\*\\*\\s(.*)\\s\\*\\*\\*\\*\\*\\s+(.*)";
        protected const string REGION_NAME_PATTERN = "^\\s*([0-9]{5,6})\\s+(.*)\\s+";

        public WildlandFire()
        {
            this.States = new StateDictionary();
        }

        public DateTime GeneratedDateTime { get; protected set; }
        public string GeneratedTimeZone { get; protected set; }
        public StateDictionary States { get; protected set; }

        public void Fetch()
        {
            // Fire off "RequestBegin" event
            if (RequestBegin != null)
            {
                RequestBegin(this, EventArgs.Empty);
            }

            WebRequest request = WebRequest.Create(DATA_URL);
            RequestState state = new RequestState(request, this);
            IAsyncResult result = request.BeginGetResponse(new AsyncCallback(ParseData), state);
        }

        private void ParseData(IAsyncResult result)
        {
            // grab the custom state object
            RequestState state = (RequestState)result.AsyncState;
            WebRequest request = (WebRequest)state.Request;
            // get the Response
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

            // Fire off "RequestComplete" event
            if (RequestComplete != null)
            {
                RequestComplete(this, EventArgs.Empty);
            }

            StreamReader reader = new StreamReader(response.GetResponseStream());

            int lineNum = 0;
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                if (lineNum == 1)
                {
                    ParseDateTime(line);
                }
                else if (Regex.IsMatch(line, STATE_PATTERN))
                {
                    ParseState(line, reader, ref lineNum);
                }

                lineNum++;
            }

            reader.Close();
            this.UpdatedDateTime = DateTime.Now;

            // Fire off "ParseComplete" event
            if (ParseComplete != null)
            {
                ParseComplete(this, EventArgs.Empty);
            }
        }

        protected void ParseDateTime(string line)
        {
            MatchCollection m = Regex.Matches(line, GENERATED_DATETIME_PATTERN);
            GroupCollection g = m[0].Groups;

            // Set date and time into `GeneratedDateTime` property
            DateTime d = DateTime.Parse(g[1].Value);
            Double hr  = Double.Parse(g[2].Value),
                   min = Double.Parse(g[3].Value);
            this.GeneratedDateTime = d.AddHours(hr).AddMinutes(min);

            // Set time zone into `GeneratedTimeZone` property
            this.GeneratedTimeZone = g[4].Value.Trim();
        }

		protected void ParseState(string line, StreamReader reader, ref int lineNum)
		{
			MatchCollection m = Regex.Matches(line, STATE_PATTERN);
			GroupCollection g = m[0].Groups;
			var s = new State();
			var columns = ParseColumnHeaders(line, line.IndexOf(g[2].Value));

			// Get this state's data rows
			string dataLine;
			bool gotInitialBlankLine = false;
			while ((dataLine = reader.ReadLine()) != null && (! gotInitialBlankLine || dataLine.Length > 0))
			{
				if (dataLine.Length < 1)
				{
					gotInitialBlankLine = true;
				}
				else
				{
					ParseStateDataRow(s, columns, dataLine);
				}

				lineNum++;
			}

			this.States.Add(g[1].Value, s);
		}

		protected ColumnHeaderList ParseColumnHeaders(string line, int start)
		{
			// Find start index
			while (start >= 0 && line[start] != ' ')
			{
				start--;
			}
			
			// Store column name and positions
			ColumnHeaderList columns = new ColumnHeaderList();
			StringBuilder str = new StringBuilder();
			bool inOpenParen = false;
			int i, j = 0;

			columns.Add(new ColumnHeader("Region", 0, start));

			for (i = start; i < line.Length; i++)
			{
				if (line[i] == '(')
				{
					inOpenParen = true;
				}
				else if (line[i] == ')')
				{
					inOpenParen = false;
				}
				
				if (line[i] == ' ' && ! inOpenParen)
				{
					string name = str.ToString();
					if (name.Length > 0)
					{
						columns.Add(new ColumnHeader(str.ToString(), j, i));
					}
					
					str.Clear();
					j = i + 1;
				}
				else
				{
					str.Append(line[i]);
				}
			}

			return columns;
		}

		protected void ParseStateDataRow(State s, ColumnHeaderList columns, string dataStr)
		{
			// Start at `dataStartIndex` and go left until you find whitespace.
			// That's the REAL start of data. This is to avoid chapping off
			// data that overflows to the left -- namely 10000+ft elevations.
			// Get the region name (non-numerical part left of `dataStartIndex`)
			Region dataDict = new Region();
			Match regionMatch = null;

			foreach (ColumnHeader col in columns)
			{
				if (col.Name == "Region")
				{
					regionMatch = Regex.Match(dataStr.Substring(col.StartIndex, col.EndIndex - col.StartIndex), REGION_NAME_PATTERN);
				}
				else
				{
					dataDict[col.Name] = dataStr.Substring(col.StartIndex, col.EndIndex - col.StartIndex);
				}
			}

			// Add to state's regions, keyed by region name.
			try
			{
				s.Add(regionMatch.Groups[2].Value.Trim(), dataDict);
			}
			catch (System.ArgumentException ex)
			{
				// Duplicate key. Put the region number in parentheses.
				// (I hate you, California)
				s.Add(regionMatch.Groups[2].Value.Trim() + " (" + regionMatch.Groups[1] + ")", dataDict);
			}
		}

        public DateTime UpdatedDateTime { get; set; }
    }
}