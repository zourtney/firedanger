﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace WildlandFire
{
    class RequestState
    {
        public RequestState(WebRequest request, WildlandFire wildlandFire)
        {
            this.Request = request;
            this.Data = wildlandFire;
        }

        public WebRequest Request { get; protected set; }
        public WildlandFire Data { get; protected set; }
    }
}
