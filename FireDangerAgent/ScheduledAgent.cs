﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using System;
using System.IO.IsolatedStorage;

namespace FireDangerAgent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            // If the Main App is Running, Toast will not show
            /*ShellToast popupMessage = new ShellToast()
            {
                Title = "My First Agent",
                Content = "Background Task Launched",
                NavigationUri = new Uri("/MainPage.xaml", UriKind.Relative)
            };
            popupMessage.Show();
            */
            this.Service = new WildlandFire.WildlandFire();
            this.Service.ParseComplete += service_ParseComplete;
            this.Service.Fetch();
        }

        void service_ParseComplete(object sender, EventArgs e)
        {
            foreach (var tile in ShellTile.ActiveTiles)
            {
                var uri = tile.NavigationUri.ToString();

                // There is an implied tile at URL "/". Ignore this one.
                if (uri.Length > 1)
                {
                    var q = Utils.ParseQueryString(uri);
                    string state = q["state"];
                    string region = q["region"];
                    var level = this.Service.States[state][region]["ADJ"].Trim();
                    StandardTileData data;

                    if (level == "V")
                    {
                        data = new StandardTileData
                        {
                            BackTitle = "Extreme",
                            BackBackgroundImage = new Uri("/Assets/extreme.png", UriKind.Relative)
                        };
                    }
                    else if (level == "H")
                    {
                        data = new StandardTileData
                        {
                            BackTitle = "High",
                            BackBackgroundImage = new Uri("/Assets/high.png", UriKind.Relative)
                        };
                    }
                    else if (level == "M")
                    {
                        data = new StandardTileData
                        {
                            BackTitle = "Moderate",
                            BackBackgroundImage = new Uri("/Assets/moderate.png", UriKind.Relative)
                        };
                    }
                    else
                    {
                        data = new StandardTileData
                        {
                            BackTitle = "Low",
                            BackBackgroundImage = new Uri("/Assets/low.png", UriKind.Relative)
                        };
                    }

                    tile.Update(data);
                }
            }

            NotifyComplete();
        }

        public WildlandFire.WildlandFire Service { get; set; }
    }
}